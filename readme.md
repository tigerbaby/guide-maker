This document-making javascript app came out of a need for documentation on a Drupal 7 project. This is a way to generate (and fairly easily maintain) a cms user guide that can be integrated into the site's admin section.

Since it is a guide making app it only goes to say that it should beget its own [wiki](http://tigerbaby.me/guide-maker-v1/site-documentation.html).