
var chapID = '#gm-chapter', backItem = null,activeItem = null, inModal = false;

function doAlert(message){
	alert(message);
}

function tag(item,type,aClass,txtHref){
	function buildELink(){
		var index = item.indexOf('-'), doc = item.substring(0,index), tmpStr = item.substring(index + 1);		
		item = tmpStr;
		aClass= ' class="' + aClass + '" data-doc="' + doc + '" ';
	}
	if(item == '')return item;	
	if(aClass != ''){
		if(aClass == 'gm-other-link') buildELink();
		else if(aClass == 'gm-external-link') aClass= ' class="' + aClass + '" target="_blank" ';		
		else aClass= ' class="' + aClass + '" ';
	}	
	if(txtHref != '')aClass += ' href="' + txtHref + '" ';	
	if(type == 'img')item = '<img src="media/' + txtHref + '" ' + aClass + '" alt="' + item + '" title="' + item + '" />';
	else item = '<' + type + aClass + '>' + item + '</' + type + '>';
	return item;
}

function cl(item){
	if(typeof console != 'undefined')console.log(item);
}

function getID(item){
	if(item==null)return item;
	var prefix = 3, index = $(item).attr('id').substring(prefix), strt = index.indexOf('-');
	index = index.substring(strt + 1);
	return index;
}

function buildChapterLinks(){
	function setNav(){
		$('.back').click(function(){
			showPage(getID(backItem),activeItem);
		});
		$('.chapters-link').click(function(){
			showPage(null,activeItem);
		});
	}
	function setALinks(item){
		$(item).each(function(){
			$(this).children('a').css('cursor','pointer');
			$(this).children('a').click(function(){
				var index = getID($(this).parent());
				showPage(index,null);
			});
		});
		if(item == 'ul.gm-chapter-links li')setALinks('ul.gm-chapter-links li ul li');
	}
	var chapCount = chapters.length,tmpStr = '',header = '',subheader = '',content = '', 
	childList = 0, childCount, childStr, secCount, tmpSec;
	for(var i = 0; i < chapCount; i++){
		header = tag(chapters[i].header,'a','','');
		childList = chapters[i].children;
		if(typeof childList != 'undefined'){
			childCount = childList.length;
			childStr = '<ul>';
			for(var ii = 0; ii < childCount; ii++){
				childStr += '<li class="gm-chapter-link" id ="gm-chaplink-' + i + '-' + ii + '">' + tag(childList[ii].header,'a','','') + '</li>';
			}
			childStr += '</ul>';
		}
		else childStr = '';
		tmpStr += '<li class="gm-chapter-link" id="gm-chaplink-' + i + '">' + header + childStr + '</li>';
		
	}	
	$('#gm-chapter-links-wrap').prepend(tag(tmpStr,'ul','gm-chapter-links',''));
	setALinks('ul.gm-chapter-links li');
	setNav();
}

function showPage(index,item){
	function buildChapter(){
		function setContent(content){
			function buildLink(tagType){
				var strtLnk = newContent.indexOf('-') + 1,
				strtTtl = newContent.indexOf('~') + 1,
				txtLnk = newContent.substring(strtLnk,(strtTtl - 1)),
				txtTtl = newContent.substring(strtTtl),
				ttlEnd = txtTtl.indexOf('~'),
				lnkClss = '',
				newLnk;
				if(txtLnk.substring(0,1)=='{'){
					var txtEnd = txtLnk.length - 1;
					txtTtl = txtLnk.substring(1,txtEnd);
					newContent = '~' + newContent.substring(0,strtTtl - 1) + '~';
					if(tagType.indexOf('a') > -1)lnkClss = 'gm-internal-link';
					else lnkClss = 'gm-other-link';
				}
				else {
					if(tagType.indexOf('a') > -1)lnkClss = 'gm-external-link';
					txtTtl = txtTtl.substring(0,ttlEnd);
					newContent = '~' + newContent.substring(0,strtTtl + ttlEnd) + '~';
				}
				if(tagType.indexOf('i') > -1)newLnk = tag(txtTtl,'img','gm-internal-img',txtLnk);
				else newLnk = tag(txtTtl,'a',lnkClss,txtLnk);
				if(content.indexOf(newContent) == -1)return false;		
				return content.replace(newContent,newLnk);			
			}

			function cycleContent(){
				strtIndex = strtIndex + 1;				
				newContent = content.substring(strtIndex);
				var tagType = newContent.substring(0,2);				
			
				if(tagType == 'a-' || tagType == 'i-' || tagType == 'e-')content = buildLink(tagType);									
				else content = content.replace('~','');
				return content;
			}

			var strtIndex = content.indexOf('~'),
			newContent = '',newLink = '';
			if(strtIndex < 0){
				tmpStr += tag(content,'div','','');
				return;
			};
			content = cycleContent();
			if(content == false)return;								
			setContent(content);
		}
		var chapCount = chapters.length,tmpStr = '',
		header = '',subheader = '',content = '', secCount, tmpSec, chapter, indexArr = [0,0],dashIndex = index.indexOf('-');
		if(dashIndex > -1){
			indexArr[0] = index.substring(0,dashIndex);
			indexArr[1] = index.substring(dashIndex + 1,index.length);
			chapter = chapters[indexArr[0]].children[indexArr[1]];
		}
		else chapter = chapters[index];
		header = tag(chapter.header,'h2','','');
		tmpStr += header;
		secCount = chapter.sections.length;
		for(var i = 0; i < secCount; i ++){
			tempSec = chapter.sections[i];
			tmpStr += tag(tempSec.header,'h3','','');
			setContent(tempSec.content);
		}
		$(chapID).html(tmpStr);
		$('a.gm-internal-link').click(function(){
			var chapterName = $(this).attr('href'),nameLen = chapterName.length,index = null;
			chapterName = chapterName.substring(1,(nameLen-1));
			$('ul.gm-chapter-links li').each(function(){
				var link = $(this).children('a').eq(0);
				if($(link).html() == chapterName)index = getID(this);
			});
			showPage(index,activeItem);
			return false;
		});
		$('a.gm-other-link').click(function(){
			if(inModal){
				doAlert('Cannot use external links in modal window.');
				return false;
			}
			var doc = $(this).attr('data-doc'),chapterName = $(this).html();
			goToDoc(doc,chapterName,'modal');
			return false;
		});
	}

	var interval = 300,menu = $('.gm-chapter-links');
	backItem = item;	
	if(index == null){
		activeItem = index;
		//$(chapID).fadeOut(interval);
		//setTimeout(function(){ $(menu).fadeIn(interval); },interval);
	}
	else {
		activeItem = '#gm-chaplink-' + index;
		$(chapID).fadeOut(interval);
		//$(menu).fadeOut(interval);		
		$('.gm-active').removeClass('gm-active');
		$(activeItem).addClass('gm-active');
		setTimeout(function(){ buildChapter(); $(chapID).fadeIn(interval); },interval);		
	}
}

function buildChapters(){
	var chapCount = chapters.length,tmpStr = '',header = '',subheader = '',content = '', secCount, tmpSec;
	for(var i = 0; i < chapCount; i++){
		header = tag(chapters[i].header,'h2','','');
		tmpStr = header;
		secCount = chapters[i].sections.length;
		for(var ii = 0; ii < secCount; ii ++){
			tempSec = chapters[i].sections[ii];
			tmpStr += tag(tempSec.header,'h3','','');
			tmpStr += tag(tempSec.content,'div','','');
		}
		$(chapID).html(tmpStr);
	}	
}

function goToDoc(doc,chapterName,target){ 
	function getDoc(){
		var docLen = documents.length,tmpStr = doc;
		for(var i = 0; i < docLen; i++){
			if(documents[i].title == tmpStr)tmpStr = documents[i].link;
		}
		return tmpStr;
	}
	chapterName = encodeURIComponent(chapterName);
	doc = getDoc();
	var src = doc + '?chapter=' + chapterName;
	if(target != 'modal')window.open(src,target); 
	else getModal(src)
}

function getModal(src){
	var wWidth = $(window).width(), mWidth = 810, center = wWidth/2, 
	mHalf = mWidth/2, posL = center - mHalf + 'px';
	if(mWidth > wWidth){
		doAlert('This browser window must be wider to accomodate modal windows.');
		return;
	}
	$('.gm-modal').css('left',posL);
	src = src + '&modal=yes';
	$('.gm-modal iframe').attr('src',src);
	window.scrollTo(0,0);
	$('.gm-screen').fadeIn();
	$('.gm-modal').fadeIn();
}

function launchApp(){
	function getQueryString(name) {
	    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	    var r = window.location.search.substr(1).match(reg);
	    if (r != null) return unescape(r[2]); return '';
	}

	function getChapter(chapter){
		var isUpdated = false;
		$('.gm-chapter-link a').each(function(){
			if($(this).html() == chapter){
				chapter = $(this).parent();
				isUpdated = true;
			}
		});
		chapter = isUpdated ? chapter : isUpdated;
		return chapter;
	}

	function setUpCloseModal(){
		function closeModal(){
			$('.gm-modal').fadeOut();
			$('.gm-screen').fadeOut();		
		}
		$('.gm-screen').click(function(){
			closeModal();
		});
		$(document).keyup(function(e){
			if(e.keyCode == 27){
				if($('.gm-screen').is(':visible'))closeModal();
			}
		});
	}
	
	function setUpModal(){
		function isModal(){
			var modal = getQueryString('modal');
			if(modal == 'yes')return true;
			else return false;
		}
		
		if(isModal()){
			$('.gm-other-documents').hide();
			$('.gm-wrapper').addClass('gm-modal-wrap');
			inModal = true;			
		}	
		else setUpCloseModal();	
	}
	
	
	function goToPage(){
		var chapter = getQueryString('chapter');
		if(chapter == '')return;
		var chapter = getChapter(chapter);
		if(!chapter)return;
		chapter = getID(chapter);
		showPage(chapter,null);
	}
	$('.gm-no-javascript').hide();
	if(typeof chapters == 'undefined' || typeof documents == 'undefined')return;
	setUpModal();
	goToPage();	
	buildChapterLinks();
	var chapter = getQueryString('chapter');
	if(chapter == '')return;	
	var chapter = getChapter(chapter);
	if(!chapter)return;
	chapter = getID(chapter);
	showPage(chapter,null);		
}

launchApp();
